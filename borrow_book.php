<?php include('header.php'); ?>
<?php
$user_id = $_POST['user_id'];
$lstbookid = $_POST['lstbookid'];
$bookname = "";
$username = "";

?>
<div class="page-title">
	<div class="title_left">
		<h3>
			<small>Trang chủ /</small> Kiểm tra lại dữ liệu mượn sách
		</h3>
	</div>
</div>



<h3>
	Người mượn
</h3>
<div class="x_content">
	<!-- content starts here -->

	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">

			<thead>
				<tr>
					<!---		<th>User Image</th>	-->
					<th>Họ và tên</th>
					<th>Số điện thoại</th>
					<th>Chứng minh thư</th>
					<th>Giới tính</th>
					<th>Địa chỉ</th>
					<th>Loại</th>
					<th>Tuổi</th>
					<th>Trạng thái</th>
					<th>Thời gian tạo</th>
					<th>Phụ Huynh</th>
					<th>Số điện thoại phụ huynh</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$result1 = mysqli_query($con, "SELECT * FROM user WHERE user_id='$user_id'") or die(mysqli_error($con));
				while ($row = mysqli_fetch_array($result1)) {
					$username = $row['firstname'] . " " . $row['lastname'];
				?>
					<tr>
						<!---		<td>
								<?php // if($row['user_image'] != ""): 
								?>
								<img src="upload/<?php // echo $row['user_image']; 
													?>" width="100px" height="100px" style="border:4px groove #CCCCCC; border-radius:5px;">
								<?php // else: 
								?>
								<img src="images/user.png" width="100px" height="100px" style="border:4px groove #CCCCCC; border-radius:5px;">
								<?php // endif; 
								?>
								</td> -->
						<td><?php echo $row['firstname'] . " " . $row['lastname']; ?></td>
						<td><?php echo $row['contact']; ?></td>
						<td><?php echo $row['school_number']; ?></td>
						<td><?php echo $row['gender']; ?></td>
						<td><?php echo $row['address']; ?></td>
						<td><?php echo $row['type']; ?></td>
						<td><?php echo $row['level']; ?></td>
						<td><?php echo $row['status']; ?></td>
						<td><?php echo date("M d, Y h:m:s a", strtotime($row['user_added'])); ?></td>
						<td><?php echo $row['parent']; ?></td>
						<td><?php echo $row['parent_number']; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<!-- content ends here -->
</div>

<h3>
	Số sách
</h3>
<div class="x_content">
	<!-- content starts here -->

	<div class="table-responsive">
		<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">

			<thead>
				<tr>
					<th style="width:100px;">Ảnh</th>
					<th>Mã vạch</th>
					<th>Tên sách</th>
					<th>Tác giả</th>
					<th>ISBN</th>
					<th>Nhà xuất bản</th>
					<th>Năm xuất bản</th>
					<th>Thể loại</th>
					<th>Trạng thái</th>
					<th>Tình trạng sách</th>
				</tr>
			</thead>
			<tbody>
				<?php

				if (isset($_GET['book_id']))
					$id = $_GET['book_id'];
				$result2 = mysqli_query($con, "SELECT * FROM book WHERE book_id in ($lstbookid)") or die(mysqli_error($con));
				while ($row = mysqli_fetch_array($result2)) {
					$bookname = $bookname . $row['book_title'] . ",";
				?>
					<tr>
						<td>
							<?php if ($row['imageName'] != "") : ?>
								<img src="upload/<?php echo $row['imageName']; ?>" width="150px" height="180px" style="border:4px groove #CCCCCC; border-radius:5px;">
							<?php else : ?>
								<img src="images/book_image.jpg" width="150px" height="180px" style="border:4px groove #CCCCCC; border-radius:5px;">
							<?php endif; ?>
						</td>
						<td><?php echo $row['book_barcode']; ?></td>
						<td style="word-wrap: break-word; width: 10em;"><?php echo $row['book_title']; ?></td>
						<td style="word-wrap: break-word; width: 10em;"><?php echo $row['author']; ?></td>
						<td><?php echo $row['isbn']; ?></td>
						<td><?php echo $row['publisher_name']; ?></td>
						<td><?php echo $row['copyright_year']; ?></td>
						<td><?php echo $row['category']; ?></td>
						<td><?php echo $row['type']; ?></td>
						<td><?php echo $row['status']; ?></td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

	<!-- content ends here -->
</div>

<div class="x_content">
	<!-- content starts here -->

	<form method="post" action="add_borrow_transaction.php" class="form-horizontal form-label-left">

		<input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
		<input type="hidden" name="lstbookid" value="<?php echo $lstbookid; ?>">

		<input type="hidden" name="bookname" value="<?php echo $bookname; ?>">
		<input type="hidden" name="username" value="<?php echo $username; ?>">
		<div class="form-group">
			<label class="control-label col-md-4" for="first-name">Ngày trả <span class="required" style="color:red;">*</span>
			</label>
			<div class="col-md-4">
				<input type="date" name="due_date" id="due_date" required="required" class="form-control col-md-7 col-xs-12">
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-md-4" for="amount">Số tiền<span class="required" style="color:red;">*</span>
			</label>
			<div class="col-md-4">
				<input type="number" name="amount" id="amount" required="required" class="form-control col-md-7 col-xs-12">
			</div>
		</div>
		<div class="ln_solid"></div>
		<div class="form-group">
			<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
				<button action="add_borrow_transaction.php" type="submit" name="submit" class="btn btn-success"><i class="fa fa-plus-square"></i> Xác nhận cho mượn</button>
			</div>
		</div>
	</form>



	<!-- content ends here -->
</div>
<?php include('footer.php'); ?>