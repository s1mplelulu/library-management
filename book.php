<?php include('header.php'); ?>

<div class="page-title">
	<div class="title_left">
		<h3>
			<small>Home /</small> Books
		</h3>
	</div>
</div>
<div class="clearfix"></div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<a href="book_print.php" target="_blank" style="background:none;">
				<button class="btn btn-danger pull-right"><i class="fa fa-print"></i>In Sách Barcode</button>
			</a>
			<a href="print_barcode1.php" target="_blank" style="background:none;">
				<button class="btn btn-danger pull-right"><i class="fa fa-print"></i> In danh sách sách</button>
			</a>
			<br />
			<br />
			<div class="x_title">
				<h2><i class="fa fa-book"></i> Danh sách sách</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a href="add_book.php" style="background:none;">
							<button class="btn btn-primary"><i class="fa fa-plus"></i> Thêm mới sách</button>
						</a>
					</li>
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					<!-- If needed 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                            </li>
						-->
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
				<ul class="nav nav-pills">
					<li role="presentation" class="active"><a href="book.php">All</a></li>
					<!-- <li role="presentation" ><a href="new_books.php">New Books</a></li>
								<li role="presentation"><a href="old_books.php">Old Books</a></li>
								<li role="presentation"><a href="lost_books.php">Lost Books</a></li>
								<li role="presentation"><a href="damage_books.php">Damaged Books</a></li>
								<li role="presentation"><a href="sub_rep.php">Subject for Replacement Books</a></li>
								<li role="presentation"><a href="hard_bound.php">Hardbound Books</a></li> -->
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<!-- content starts here -->

				<div class="table-responsive">
					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

						<thead>
							<tr>
								<th>ID</th>
								<th style="width:100px;">Ảnh</th>
								<th>Mã Code</th>
								<th>Tên sách</th>
								<!-- <th>ISBN</th> -->
								<th>Tác giả</th>
								<th>Nhà xuất bản</th>
								<th>Thể loại</th>
								<th>Trạng thái</th>
								<th>Tình trạng sách</th>
								<th>Ngôn ngữ</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>

							<?php
							$result = mysqli_query($con, "select * from book order by book_id DESC ") or die(mysqli_error($con));
							while ($row = mysqli_fetch_array($result)) {
								$id = $row['book_id'];
								$category_id = $row['category'];

								$cat_query = mysqli_query($con, "select * from category where category_id = '$category_id'") or die(mysqli_error($con));
								$cat_row = mysqli_fetch_array($cat_query);
							?>
								<tr>
									<td><?php echo $row['book_id']; ?></td>
									<td>
										<?php if ($row['imageName'] != "") : ?>
											<img src="upload/<?php echo $row['imageName']; ?>" class="img-thumbnail" width="75px" height="50px">
										<?php else : ?>
											<img src="images/book_image.jpg" class="img-thumbnail" width="75px" height="50px">
										<?php endif; ?>
									</td>
									<!-- <td><a target="_blank" href="view_book_barcode.php?code=<?php echo $row['book_barcode']; ?>"><?php echo $row['book_barcode']; ?></a></td> -->
									<td><a target="_blank" href="print_barcode_individual1.php?code=<?php echo $row['book_barcode']; ?>"><?php echo $row['book_barcode']; ?></a></td>
									<td style="word-wrap: break-word; width: 10em;"><?php echo $row['book_title']; ?></td>
									<!-- <td style="word-wrap: break-word; width: 10em;"><?php echo $row['isbn']; ?></td> -->
									<td style="word-wrap: break-word; width: 10em;"><?php echo $row['author']; ?></td>
									<td style="word-wrap: break-word; width: 10em;"><?php echo $row['publisher_name']; ?></td>
									<td><?php echo $row['category']; ?></td>
									<td><?php echo $row['type']; ?></td>
									<td><?php echo $row['status']; ?></td>
									<td><?php echo $row['language']; ?></td>
									<td>
										<a class="btn btn-primary" for="ViewAdmin" href="view_book.php<?php echo '?book_id=' . $id; ?>">
											<i class="fa fa-search"></i>
										</a>
										<a class="btn btn-warning" for="ViewAdmin" href="edit_book.php<?php echo '?book_id=' . $id; ?>">
											<i class="fa fa-edit"></i>
										</a>
										<a class="btn btn-danger" for="DeleteAdmin" href="#delete<?php echo $id; ?>" data-toggle="modal" data-target="#delete<?php echo $id;
																																								?>">
											<i class="glyphicon glyphicon-trash icon-white"></i>
										</a>


										<!-- delete modal user -->
										<div class="modal fade" id="delete<?php echo $id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
											<div class="modal-dialog">
												<div class="modal-content">
													<div class="modal-header">
														<h4 class="modal-title" id="myModalLabel"><i class="glyphicon glyphicon-user"></i> User</h4>
													</div>
													<div class="modal-body">
														<div class="alert alert-danger">
															Bạn có chắc muốn xóa sách này không?
														</div>
														<div class="modal-footer">
															<button class="btn btn-inverse" data-dismiss="modal" aria-hidden="true"><i class="glyphicon glyphicon-remove icon-white"></i> No</button>
															<a href="delete_book.php<?php echo '?book_id=' . $id; ?>" style="margin-bottom:5px;" class="btn btn-primary"><i class="glyphicon glyphicon-ok icon-white"></i> Yes</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>

				<!-- content ends here -->
			</div>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>