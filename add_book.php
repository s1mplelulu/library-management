<?php
include('header.php');


$query = mysqli_query($con, "SELECT * FROM `barcode` ORDER BY mid_barcode DESC ") or die(mysqli_error($con));
$fetch = mysqli_fetch_array($query);
$mid_barcode = $fetch['mid_barcode'];

$new_barcode =  $mid_barcode + 1;
$pre_barcode = "VNHS";
$suf_barcode = "LMS";
$generate_barcode = $pre_barcode . $new_barcode . $suf_barcode;
?>


<div class="page-title">
    <div class="title_left">
        <h3>
            <small>Trang chủ / Sách /</small> Thêm sách
        </h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-plus"></i> Thêm sách</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <!-- If needed 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                            </li>
						-->
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- content starts here -->

                <form method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
                    <input type="hidden" name="new_barcode" value="<?php echo $new_barcode; ?>">

                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Tên sách <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="book_title" id="first-name2" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Tác giả<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="author" id="first-name2" required="required" class="form-control col-md-7 col-xs-12">
                            <!-- <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-plus-square"></i> Thêm mới</button> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Nhà xuất bản
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="publisher_name" id="last-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Ngôn ngữ<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="language" class="select2_single form-control" tabindex="-1" required="required">
                                <option value="Việt Nam">Việt Nam</option>
                                <option value="English">English</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">ISBN
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="isbn" id="last-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Năm xuất bản
                        </label>
                        <div class="col-md-4">
                            <input type="number" required="required" name="copyright_year" id="last-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Trạng thái <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="status" class="select2_single form-control" tabindex="-1" required="required">
                                <option value="New">Mới</option>
                                <option value="Old">Cũ</option>
                                <option value="Lost">Mất</option>
                                <option value="LikeOld">Hơi cũ</option>
                                <option value="Replacement">Được thay thế</option>
                                <option value="LikeNew">Như mới</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Thể loại <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="category_id" class="select2_single form-control" tabindex="-1" required="required">
                                <?php
                                $result = mysqli_query($con, "select * from category") or die(mysqli_error($con));
                                while ($row = mysqli_fetch_array($result)) {
                                    $id = $row['category_id'];
                                ?>
                                    <option value="<?php echo $row['classname']; ?>"><?php echo $row['classname']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Ảnh<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="file" required="required" style="height:44px;" name="image" id="last-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <a href="book.php"><button type="button" class="btn btn-primary"><i class="fa fa-times-circle-o"></i> Quay lại</button></a>
                            <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-plus-square"></i> Thêm mới</button>
                        </div>
                    </div>
                </form>

                <?php

                // include ('include/dbcon.php');
                if (!isset($_FILES['image']['tmp_name'])) {
                    echo "";
                } else {
                    //$file = $_FILES['image']['tmp_name'];
                    //$image = $_FILES["image"]["name"];
                    $image_name = addslashes($_FILES['image']['name']);
                    // $imgData = addslashes(file_get_contents($_FILES['image']['tmp_name']));
                    // $imageProperties = getimageSize($_FILES['image']['tmp_name']);
                    $size = $_FILES["image"]["size"];
                    $error = $_FILES["image"]["error"]; {
                        if ($size > 1000000) //conditions for the file
                        {
                            die("Format is not allowed or file size is too big!");
                        } else {

                            move_uploaded_file($_FILES["image"]["tmp_name"], "upload/" . $_FILES["image"]["name"]);
                            $book_image = $_FILES["image"]["name"];

                            $book_title = $_POST['book_title'];
                            $category_id = $_POST['category_id'];
                            $author = $_POST['author'];

                            $language = $_POST['language'];
                            //$book_pub = $_POST['book_pub'];
                            $publisher_name = $_POST['publisher_name'];
                            $isbn = $_POST['isbn'];
                            $copyright_year = $_POST['copyright_year'];
                            $status = $_POST['status'];


                            $pre = "VNHS";
                            $mid = $_POST['new_barcode'];
                            $suf = "LMS";
                            $gen = $pre . $mid . $suf;
                            //         mysqli_query($con, "insert into book (book_title,category,author,language,publisher_name,isbn,copyright_year,status,book_barcode,date_added,imageType,image)
                            // values('$book_title','$category_id','$author','$language','$publisher_name','$isbn','$copyright_year','$status','$gen',NOW(),'{$imageProperties['mime']}','{$imgData}')") or die(mysqli_error($con));

                            mysqli_query($con, "insert into book (book_title,category,author,language,publisher_name,isbn,copyright_year,status,book_barcode,date_added,imageName,type)
					values('$book_title','$category_id','$author','$language','$publisher_name','$isbn','$copyright_year','$status','$gen',NOW(),'$image_name')",'BORROWED') or die(mysqli_error($con));


                            mysqli_query($con, "insert into barcode (pre_barcode,mid_barcode,suf_barcode) values ('$pre', '$mid', '$suf') ") or die(mysqli_error($con));
                            echo "<script>alert('Thêm mới thành công!'); window.location='book.php'</script>";
                        }
                    }
                }
                ?>

                <!-- content ends here -->
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>