<?php include('header.php'); ?>

<div class="page-title">
    <div class="title_left">
        <h3>
            <small>Trang chủ /</small> Mượn sách
        </h3>
    </div>
</div>
<div class="clearfix"></div>
<script>
    var lstbookid = new Array();
    Array.prototype.remove = function() {
        var what, a = arguments,
            L = a.length,
            ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };
</script>

<div class="row">
    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content">
                <label class="control-label" for="first-name">Chọn người<span class="required" style="color:red;">*</span>
                    <form method="post" action="borrow_book.php">
                        <select name="user_id" class="select2_single form-control" required="required" tabindex="-1">

                            <?php

                            $result = mysqli_query($con, "select * from user where status = 'Hoạt động' ") or die(mysqli_error($con));
                            while ($row = mysqli_fetch_array($result)) {
                                $id = $row['user_id'];
                            ?>
                                <option value="<?php echo $row['user_id']; ?>"><?php echo $row['contact']; ?> - <?php echo $row['firstname'] . " " . $row['lastname']; ?></option>
                            <?php } ?>
                        </select>

                        <input type="hidden" name="lstbookid" id="lstbookid">
                        <br />
                        <br />
                        <button name="submit" type="submit" action="borrow_book.php" class="btn btn-primary" style="margin-left:110px;"><i class="glyphicon glyphicon-log-in"></i> Submit</button>
                    </form>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_content" id="bookdiv">
                <label class="control-label col-md-4">Sách:
                    <br />
                    <br />

            </div>
        </div>
    </div>
</div>

<div class="x_content">
    <div class="x_panel">

        <div class="table-responsive">
            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">

                <thead>
                    <tr>
                        <th>ID</th>
                        <th style="width:100px;">Ảnh</th>
                        <th>Mã Code</th>
                        <th>Tên sách</th>
                        <!-- <th>ISBN</th> -->
                        <th>Tác giả</th>
                        <th>Nhà xuất bản</th>
                        <th>Thể loại</th>
                        <th>Trạng thái</th>
                        <th>Tình trạng sách</th>
                        <th>Ngôn ngữ</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $result = mysqli_query($con, "select * from book order by book_id DESC ") or die(mysqli_error($con));
                    while ($row = mysqli_fetch_array($result)) {
                        $id = $row['book_id'];
                        $category_id = $row['category'];

                        // $cat_query = mysqli_query($con, "select * from category where category_id = '$category_id'") or die(mysqli_error($con));
                        // $cat_row = mysqli_fetch_array($cat_query);
                    ?>
                        <tr>
                            <td><?php echo $row['book_id']; ?></td>
                            <td>
                                <?php if ($row['imageName'] != "") : ?>
                                    <img src="upload/<?php echo $row['imageName']; ?>" class="img-thumbnail" width="75px" height="50px">
                                <?php else : ?>
                                    <img src="images/book_image.jpg" class="img-thumbnail" width="75px" height="50px">
                                <?php endif; ?>
                            </td>
                            <!-- <td><a target="_blank" href="view_book_barcode.php?code=<?php echo $row['book_barcode']; ?>"><?php echo $row['book_barcode']; ?></a></td> -->
                            <td><a target="_blank" href="print_barcode_individual1.php?code=<?php echo $row['book_barcode']; ?>"><?php echo $row['book_barcode']; ?></a></td>
                            <td style="word-wrap: break-word; width: 10em;"><?php echo $row['book_title']; ?></td>
                            <!-- <td style="word-wrap: break-word; width: 10em;"><?php echo $row['isbn']; ?></td> -->
                            <td style="word-wrap: break-word; width: 10em;"><?php echo $row['author']; ?></td>
                            <td style="word-wrap: break-word; width: 10em;"><?php echo $row['publisher_name']; ?></td>
                            <td><?php echo $row['category']; ?></td>
                            <td><?php echo $row['type']; ?></td>
                            <td><?php echo $row['status']; ?></td>
                            <td><?php echo $row['language']; ?></td>
                            <td>
                                <button type="button" value="<?php echo $row['book_id']; ?>" id="<?php echo $row['book_id']; ?>" onclick="document.getElementById('<?php echo $row['book_id']; ?>').disabled = true;lstbookid.push(<?php echo $row['book_id']; ?>);document.getElementById('lstbookid').value = lstbookid;" )>Chọn sách này</button>
                            </td>
                        </tr>
                        <script>
                            $(document).ready(function() {
                                var he = $("#<?php echo $row['book_id']; ?>").value;
                                $("#<?php echo $row['book_id']; ?>").click(function() {
                                    var id = <?php echo $row['book_id']; ?>;
                                    var newlabel = document.createElement("Label");
                                    newlabel.setAttribute("id", "label" + id);
                                    newlabel.innerHTML = "<?php echo $row['book_title']; ?>";
                                    document.getElementById("bookdiv").appendChild(newlabel);

                                    var newButton = document.createElement("Button");
                                    newButton.setAttribute("id", "button" + id);
                                    newButton.innerHTML = "Xóa sách này";
                                    newButton.setAttribute("onClick", "document.getElementById('label" + id + "').remove();document.getElementById('button" + id + "').remove();document.getElementById('br" + id + "').remove();document.getElementById(" + id + ").disabled = false;lstbookid.remove(" + id + ");document.getElementById('lstbookid').value = lstbookid;");
                                    document.getElementById("bookdiv").appendChild(newButton);

                                    var newBR = document.createElement("BR");
                                    newBR.setAttribute("id", "br" + id);
                                    document.getElementById("bookdiv").appendChild(newBR);
                                });
                            });
                        </script>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- content ends here -->
</div>

<?php include('footer.php'); ?>