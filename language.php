<?php 
$languageString["contact"] = "Số điện thoại";
$languageString["parentNumber"] = "Số điện thoại bố mẹ";
$languageString["fullName"] = "Tên người mượn";
$languageString["type"] = "Loại";
$languageString["schoolId"] = "Mã số người mượn";
$languageString["home"] = "Trang chủ";

$languageString["status"] = "Trạng thái";
$languageString["action"] = "Thao tác";
$languageString["search"] = "Tìm kiếm";
$languageString["member"] = "Người mượn";
$languageString["lstMember"] = "Danh sách người mượn";
$languageString["addMember"] = "Thêm người mượn";
$languageString["importMember"] = "Nhập người mượn";
$languageString["printMemberBarcode"] = "In mã";
$languageString["printMember"] = "In danh sách";
$languageString["addMember"] = "Thêm người mượn";
$languageString["gender"] = "Giới tính";


$languageString["recordsPerPage"] = "Dữ liệu mỗi trang";
$languageString["previous"] = "Trước";
$languageString["next"] = "Sau";
$languageString["yes"] = "Có";
$languageString["no"] = "Không";
$languageString["sureDelete"] = "Bạn có chắc chắn muốn xóa?";
$languageString["homeLib"] = "Quản lý thư viện";
$languageString["nameLib"] = "Bến Trăng";

$languageString["data"] = "Dữ liệu";
$languageString["book"] = "Sách";
$languageString["admin"] = "Quản lý";
$languageString["transaction"] = "Giao dịch";

$languageString["borrow"] = "Mượn sách";
$languageString["return"] = "Trả sách";
$languageString["borrowHistory"] = "Lịch sử mượn sách";
$languageString["returnHistory"] = "Lịch sử trả sách";
$languageString["aboutUs"] = "Giới thiệu";
?>