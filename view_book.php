<?php include('header.php'); ?>

<div class="page-title">
	<div class="title_left">
		<h3>
			<small>Trang chủ / Sách</small> / Xem chi tiết
		</h3>
	</div>
</div>
<div class="clearfix"></div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><i class="fa fa-info"></i> Xem chi tiết</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li>
						<a href="book.php" style="background:none;">
							<button class="btn btn-primary"><i class="fa fa-arrow-left"></i> Quay lại</button>
						</a>
					</li>
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
					<!-- If needed 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                            </li>
						-->
					<li><a class="close-link"><i class="fa fa-close"></i></a></li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<!-- content starts here -->

				<div class="table-responsive">
					<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered">

						<thead>
							<tr>
								<th style="width:100px;">Ảnh</th>
								<th>Mã vạch</th>
								<th>Tên sách</th>
								<th>Tác giả</th>
								<th>ISBN</th>
								<th>Nhà xuất bản</th>
								<th>Năm xuất bản</th>
								<th>Thể loại</th>
								<th>Trạng thái</th>
								<th>Tình trạng sách</th>
							</tr>
						</thead>
						<tbody>
							<?php

							if (isset($_GET['book_id']))
								$id = $_GET['book_id'];
							$result1 = mysqli_query($con, "SELECT * FROM book WHERE book_id='$id'");
							while ($row = mysqli_fetch_array($result1)) {
							?>
								<tr>
									<td>
										<?php if ($row['imageName'] != "") : ?>
											<img src="upload/<?php echo $row['imageName']; ?>" width="150px" height="180px" style="border:4px groove #CCCCCC; border-radius:5px;">
										<?php else : ?>
											<img src="images/book_image.jpg" width="150px" height="180px" style="border:4px groove #CCCCCC; border-radius:5px;">
										<?php endif; ?>
									</td>
									<td><?php echo $row['book_barcode']; ?></td>
									<td style="word-wrap: break-word; width: 10em;"><?php echo $row['book_title']; ?></td>
									<td style="word-wrap: break-word; width: 10em;"><?php echo $row['author']; ?></td>
									<td><?php echo $row['isbn']; ?></td>
									<td><?php echo $row['publisher_name']; ?></td>
									<td><?php echo $row['copyright_year']; ?></td>
									<td><?php echo $row['category']; ?></td>
									<td><?php echo $row['type']; ?></td>
									<td><?php echo $row['status']; ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>

				<!-- content ends here -->
			</div>
		</div>
	</div>
</div>

<?php include('footer.php'); ?>