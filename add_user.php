<?php include('header.php'); ?>

<div class="page-title">
    <div class="title_left">
        <h3>
            <small>Trang chủ / Người mượn</small>
        </h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <!-- If needed 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                            </li>
						-->
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- content starts here -->

                <form method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Chứng minh thư
                        </label>
                        <div class="col-md-2">
                            <input type="number" name="school_number" id="first-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Họ
                        </label>
                        <div class="col-md-3">
                            <input type="text" name="firstname" placeholder="Họ....." id="first-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Tên<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-3">
                            <input type="text" name="lastname" placeholder="Tên....." id="last-name2" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="contact-name">Số điện thoại<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-3">
                            <input type="tel" autocomplete="off" maxlength="11" name="contact" id="contact" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Giới tính <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="gender" class="select2_single form-control" required="required" tabindex="-1">
                                <option value="Nam">Nam</option>
                                <option value="Nữ">Nữ</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="addresse">Địa chỉ<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="address" id="address" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Phân loại <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="type" class="select2_single form-control" required="required" tabindex="-1">
                                <option value="Trẻ con">Trẻ con</option>
                                <option value=">Người lớn">Người lớn</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="type">Tuổi <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-2">
                            <input type="number" name="level" id="first-name2" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="parent">Phụ huynh
                        </label>
                        <div class="col-md-3">
                            <input type="text" name="parent" placeholder="Phụ huynh....." id="first-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Số điện thoại phụ huynh
                        </label>
                        <div class="col-md-3">
                            <input type="text" name="parent_number" placeholder="Số điện thoại....." id="first-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <!---        <div class="form-group">
                                    <label class="control-label col-md-4" for="last-name">User Image <span class="required">*</span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="file" style="height:44px;" name="image" id="last-name2" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>	-->
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <a href="user.php"><button type="button" class="btn btn-primary"><i class="fa fa-times-circle-o"></i> Hủy</button></a>
                            <button type="submit" name="submit" class="btn btn-success"><i class="fa fa-plus-square"></i> Thêm mới</button>
                        </div>
                    </div>
                </form>

                <?php
                // include ('include/dbcon.php');
                if (isset($_POST['submit'])) {

                    //					if (!isset($_FILES['image']['tmp_name'])) {
                    //					echo "";
                    //					}else{
                    //					$file=$_FILES['image']['tmp_name'];
                    //					$image = $_FILES["image"] ["name"];
                    //					$image_name= addslashes($_FILES['image']['name']);
                    //					$size = $_FILES["image"] ["size"];
                    //					$error = $_FILES["image"] ["error"];
                    //					
                    //					{
                    //								if($size > 10000000) //conditions for the file
                    //								{
                    //								die("Format is not allowed or file size is too big!");
                    //								}
                    //								
                    //							else
                    //								{
                    //
                    //							move_uploaded_file($_FILES["image"]["tmp_name"],"upload/" . $_FILES["image"]["name"]);			
                    //							$profile=$_FILES["image"]["name"];
                    $school_number = $_POST['school_number'];
                    $firstname = $_POST['firstname'];
                    $lastname = $_POST['lastname'];
                    $contact = $_POST['contact'];
                    $gender = $_POST['gender'];
                    $address = $_POST['address'];
                    $type = $_POST['type'];
                    $level = $_POST['level'];
                    $parent = $_POST['parent'];
                    $parent_number = $_POST['parent_number'];

                    $result = mysqli_query($con, "select * from user WHERE school_number='$school_number' ") or die(mysqli_error($con));
                    $row = mysqli_num_rows($result);
                    if ($row > 0) {
                        echo "<script>alert('ID Number already active!'); window.location='user.php'</script>";
                    } else {
                        mysqli_query($con, "insert into user (school_number,firstname, parent, parent_number, lastname, contact, gender, address, type, level, status, user_added,user_image)
						values ('$school_number','$firstname', '$parent', '$parent_number', '$lastname', '$contact', '$gender', '$address', '$type', '$level', 'Hoạt động', NOW(),'user.png')") or die(mysqli_error($con));
                        echo "<script>alert('Thêm mới thành công!'); window.location='user.php'</script>";
                    }
                    //						}
                    //						}
                }
                ?>

                <!-- content ends here -->
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>