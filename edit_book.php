<?php include('include/dbcon.php');
$ID = $_GET['book_id'];
?>
<?php include('header.php'); ?>

<div class="page-title">
    <div class="title_left">
        <h3>
            <small>Trang chủ / Sách /</small> Sửa thông tin sách
        </h3>
    </div>
</div>
<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><i class="fa fa-pencil"></i> Sửa thông tin</h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    <!-- If needed 
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <i class="fa fa-wrench"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="#">Settings 1</a></li>
                                    <li><a href="#">Settings 2</a></li>
                                </ul>
                            </li>
						-->
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <!-- content starts here -->
                <?php
                $query1 = mysqli_query($con, "select * from book where book_id='$ID'") or die(mysqli_error());
                $row = mysqli_fetch_assoc($query1);
                ?>

                <form method="post" enctype="multipart/form-data" class="form-horizontal form-label-left">
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Ảnh
                        </label>
                        <div class="col-md-4">
                            <a href=""><?php if ($row['imageName'] != "") : ?>
                                    <img src="upload/<?php echo $row['imageName']; ?>" width="100px" height="100px" style="border:4px groove #CCCCCC; border-radius:5px;">
                                <?php else : ?>
                                    <img src="images/book_image.jpg" width="100px" height="100px" style="border:4px groove #CCCCCC; border-radius:5px;">
                                <?php endif; ?>
                            </a>
                            <!-- <input type="file" style="height:44px; margin-top:10px;" name="image" id="last-name2" class="form-control col-md-7 col-xs-12" /> -->
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Tên sách <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="book_title" value="<?php echo $row['book_title']; ?>" id="first-name2" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="first-name">Tác giả <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="author" id="first-name2" value="<?php echo $row['author']; ?>" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Nhà xuất bản
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="publisher_name" value="<?php echo $row['publisher_name']; ?>" id="last-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Ngôn ngữ<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="language" class="select2_single form-control" tabindex="-1" required="required">
                                <option value="<?php echo $row['language']; ?>"><?php echo $row['language']; ?></option>
                                <option value="Việt Nam">Việt Nam</option>
                                <option value="English">English</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">ISBN
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="isbn" id="last-name2" value="<?php echo $row['isbn']; ?>" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Năm xuất bản
                        </label>
                        <div class="col-md-4">
                            <input type="number" name="copyright_year" value="<?php echo $row['copyright_year']; ?>" id="last-name2" class="form-control col-md-7 col-xs-12">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Trạng thái <span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="status" class="select2_single form-control" tabindex="-1" required="required">
                                <option value="<?php echo $row['status']; ?>"><?php echo $row['status']; ?></option>
                                <option value="New">Mới</option>
                                <option value="Old">Cũ</option>
                                <option value="Lost">Mất</option>
                                <option value="LikeOld">Hơi cũ</option>
                                <option value="Replacement">Được thay thế</option>
                                <option value="LikeNew">Như mới</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4" for="last-name">Thể loại<span class="required" style="color:red;">*</span>
                        </label>
                        <div class="col-md-4">
                            <select name="category_id" class="select2_single form-control" tabindex="-1" required="required">
                                <option value="<?php echo $row['category']; ?>"><?php echo $row['category']; ?></option>
                                <?php
                                $result = mysqli_query($con, "select * from category") or die(mysqli_error($con));
                                while ($row2 = mysqli_fetch_array($result)) {
                                    $id = $row2['category_id'];
                                ?>
                                    <option value="<?php echo $row2['classname']; ?>"><?php echo $row2['classname']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                            <a href="book.php"><button type="button" class="btn btn-primary"><i class="fa fa-times-circle-o"></i> Quay lại</button></a>
                            <button type="submit" name="update11" class="btn btn-success"><i class="glyphicon glyphicon-save"></i> Sửa</button>
                        </div>
                    </div>
                </form>

                <?php
                $id = $_GET['book_id'];
                if (isset($_POST['update11'])) {
                    // $image = $_FILES["image"]["name"];
                    // $image_name = addslashes($_FILES['image']['name']);
                    // $size = $_FILES["image"]["size"];
                    // $error = $_FILES["image"]["error"];




                    // $book_image = $_FILES["image"]["name"];

                    $book_title = $_POST['book_title'];
                    $category_id = $_POST['category_id'];
                    $author = $_POST['author'];

                    $language = $_POST['language'];
                    //$book_pub = $_POST['book_pub'];
                    $publisher_name = $_POST['publisher_name'];
                    $isbn = $_POST['isbn'];
                    $copyright_year = $_POST['copyright_year'];
                    $status = $_POST['status'];
                    // $still_profile1 = $row['book_image'];




                    mysqli_query($con, " UPDATE book SET book_title='$book_title', category='$category_id',
                        publisher_name='$publisher_name', isbn='$isbn', copyright_year='$copyright_year', status='$status' WHERE book_id = '$id' ") or die(mysqli_error($con));
                    echo "<script>alert('Sửa thành công!'); window.location='book.php'</script>";
                }
                ?>

                <!-- content ends here -->
            </div>
        </div>
    </div>
</div>

<?php include('footer.php'); ?>